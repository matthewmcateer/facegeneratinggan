# facegeneratingGAN
Generative Adversarial Network designed to produce realistic human faces after being trained on a sample of 200,000+ images of human faces

### What is this repository for? ###

* This repository is for the Jupyter notebook and supporting python files
* The data files used for this project can be found either in my Bitbucket (as it was too much for my Github) or at the websites for the [MNIST](http://yann.lecun.com/exdb/mnist/) and [CelebA](http://mmlab.ie.cuhk.edu.hk/projects/CelebA.html) datasets.
* Version: stable

### How do I get set up? ###

* To set up, clone the repository or download a .zip file with the repository to your desired directory. Make sure that the directory contains a folder titled "data" to include the MNIST and CelebA folders in.
* Configuration
* The helper.py and unittests.py files are dependencies that should also be included in the directory
* Unit tests are set up to automatically be run by the python notebook before the running of the Genrative Adversarial Network.
* To start, run the Jupyter Notebook application in an environment (such as that created by Anaconda) for 

### Contribution guidelines ###

* This repository can easily be forked and contributed to on Github. Feel free to send a pull request.